Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: astLib
Source: https://astlib.sourceforge.net/

Files: * PyWCSTools/wcssubs-*/*.i PyWCSTools/wcssubs-*/*_wrap.c
Copyright: 2007-2013 Matt Hilton, Steven Boada
 2014-2015 Gijs Molenaar <gijs@pythonic.nl>, Ole Streicher <olebole@debian.org>
License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: PyWCSTools/wcssubs-*/*
Copyright: Copyright (C) 1995-2002, Mark Calabretta
 Copyright (C) 1994-2011 Associated Universities, Inc. Washington DC, USA.
 Copyright (C) 1995-2012 Smithsonian Astrophysical Observatory, Cambridge, MA, USA
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
 .
 Note that these files are not used in the package build process, so the
 copyright information is included here only as a reference.
